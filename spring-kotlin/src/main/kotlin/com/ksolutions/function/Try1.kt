package com.ksolutions.function

fun main(args: Array<String>) {
    println("Hello")
    printMessage("Dexter")
    printMessage((sum(5,4).toString()))
}

fun printMessage(param: String){
    println("$param is the parameter")
}

fun sum(x: Int, y: Int) = x + y