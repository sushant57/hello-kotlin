package com.ksolutions.function

class OverloadedMethods {
    fun printMessage(message: String, prefix: String = "", suffix: String = ""): String {
        return prefix + message + suffix;
    }
}

fun main(args: Array<String>) {
    val overloaded = OverloadedMethods().printMessage("message", "prefix:", ":theEnd")
    println(overloaded)

    val overloaded1 = OverloadedMethods().printMessage("messageWithout")
    println(overloaded1)
}